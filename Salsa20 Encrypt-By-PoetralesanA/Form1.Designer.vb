﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnEncrypt = New System.Windows.Forms.Button()
        Me.btnDecrypt = New System.Windows.Forms.Button()
        Me.txtPlainText = New System.Windows.Forms.TextBox()
        Me.txtEncryptedText = New System.Windows.Forms.TextBox()
        Me.txtDecryptedText = New System.Windows.Forms.TextBox()
        Me.lblPlainText = New System.Windows.Forms.Label()
        Me.lblEncryptedText = New System.Windows.Forms.Label()
        Me.lblDecryptedText = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnEncrypt
        '
        Me.btnEncrypt.BackColor = System.Drawing.Color.LightSlateGray
        Me.btnEncrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEncrypt.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEncrypt.ForeColor = System.Drawing.Color.White
        Me.btnEncrypt.Location = New System.Drawing.Point(19, 459)
        Me.btnEncrypt.Name = "btnEncrypt"
        Me.btnEncrypt.Size = New System.Drawing.Size(75, 30)
        Me.btnEncrypt.TabIndex = 0
        Me.btnEncrypt.Text = "Encrypt"
        Me.btnEncrypt.UseVisualStyleBackColor = False
        '
        'btnDecrypt
        '
        Me.btnDecrypt.BackColor = System.Drawing.Color.DarkCyan
        Me.btnDecrypt.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDecrypt.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDecrypt.ForeColor = System.Drawing.Color.White
        Me.btnDecrypt.Location = New System.Drawing.Point(295, 459)
        Me.btnDecrypt.Name = "btnDecrypt"
        Me.btnDecrypt.Size = New System.Drawing.Size(75, 30)
        Me.btnDecrypt.TabIndex = 1
        Me.btnDecrypt.Text = "Decrypt"
        Me.btnDecrypt.UseVisualStyleBackColor = False
        '
        'txtPlainText
        '
        Me.txtPlainText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPlainText.Location = New System.Drawing.Point(19, 180)
        Me.txtPlainText.Multiline = True
        Me.txtPlainText.Name = "txtPlainText"
        Me.txtPlainText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtPlainText.Size = New System.Drawing.Size(378, 113)
        Me.txtPlainText.TabIndex = 2
        Me.txtPlainText.Text = "Yt : BrokecoderZ" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Facebook : PoetralesanA"
        '
        'txtEncryptedText
        '
        Me.txtEncryptedText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEncryptedText.Location = New System.Drawing.Point(19, 353)
        Me.txtEncryptedText.Multiline = True
        Me.txtEncryptedText.Name = "txtEncryptedText"
        Me.txtEncryptedText.ReadOnly = True
        Me.txtEncryptedText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtEncryptedText.Size = New System.Drawing.Size(250, 100)
        Me.txtEncryptedText.TabIndex = 3
        '
        'txtDecryptedText
        '
        Me.txtDecryptedText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtDecryptedText.Location = New System.Drawing.Point(295, 353)
        Me.txtDecryptedText.Multiline = True
        Me.txtDecryptedText.Name = "txtDecryptedText"
        Me.txtDecryptedText.ReadOnly = True
        Me.txtDecryptedText.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txtDecryptedText.Size = New System.Drawing.Size(250, 100)
        Me.txtDecryptedText.TabIndex = 4
        '
        'lblPlainText
        '
        Me.lblPlainText.AutoSize = True
        Me.lblPlainText.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPlainText.Location = New System.Drawing.Point(16, 160)
        Me.lblPlainText.Name = "lblPlainText"
        Me.lblPlainText.Size = New System.Drawing.Size(65, 15)
        Me.lblPlainText.TabIndex = 5
        Me.lblPlainText.Text = "Plain Text:"
        '
        'lblEncryptedText
        '
        Me.lblEncryptedText.AutoSize = True
        Me.lblEncryptedText.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEncryptedText.Location = New System.Drawing.Point(16, 333)
        Me.lblEncryptedText.Name = "lblEncryptedText"
        Me.lblEncryptedText.Size = New System.Drawing.Size(138, 15)
        Me.lblEncryptedText.TabIndex = 6
        Me.lblEncryptedText.Text = "Output Encrypted Text:"
        '
        'lblDecryptedText
        '
        Me.lblDecryptedText.AutoSize = True
        Me.lblDecryptedText.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDecryptedText.Location = New System.Drawing.Point(292, 333)
        Me.lblDecryptedText.Name = "lblDecryptedText"
        Me.lblDecryptedText.Size = New System.Drawing.Size(141, 15)
        Me.lblDecryptedText.TabIndex = 7
        Me.lblDecryptedText.Text = "Output Decrypted Text:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.Salsa20_Encrypt_By_PoetralesanA.My.Resources.Resources._6041736
        Me.PictureBox1.Location = New System.Drawing.Point(232, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(97, 79)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 8
        Me.PictureBox1.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.LightSlateGray
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(100, 459)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 30)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Clear"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.DarkCyan
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(376, 459)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 30)
        Me.Button2.TabIndex = 11
        Me.Button2.Text = "Clear"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Khaki
        Me.Panel1.Controls.Add(Me.LinkLabel2)
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(561, 143)
        Me.Panel1.TabIndex = 12
        '
        'LinkLabel2
        '
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel2.LinkColor = System.Drawing.Color.DodgerBlue
        Me.LinkLabel2.Location = New System.Drawing.Point(3, 101)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(55, 13)
        Me.LinkLabel2.TabIndex = 11
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Facebook"
        '
        'LinkLabel1
        '
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LinkLabel1.LinkColor = System.Drawing.Color.Red
        Me.LinkLabel1.Location = New System.Drawing.Point(3, 120)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(47, 13)
        Me.LinkLabel1.TabIndex = 10
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Youtube"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 8.25!, CType(((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic) _
                Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(229, 94)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 26)
        Me.Label1.TabIndex = 9
        Me.Label1.Text = "Salsha20 Encryptor" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "By PoetralesanA"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(561, 526)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblDecryptedText)
        Me.Controls.Add(Me.lblEncryptedText)
        Me.Controls.Add(Me.lblPlainText)
        Me.Controls.Add(Me.txtDecryptedText)
        Me.Controls.Add(Me.txtEncryptedText)
        Me.Controls.Add(Me.txtPlainText)
        Me.Controls.Add(Me.btnDecrypt)
        Me.Controls.Add(Me.btnEncrypt)
        Me.Controls.Add(Me.Panel1)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Salsa20 Encryption/Decryption"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnEncrypt As System.Windows.Forms.Button
    Friend WithEvents btnDecrypt As System.Windows.Forms.Button
    Friend WithEvents txtPlainText As System.Windows.Forms.TextBox
    Friend WithEvents txtEncryptedText As System.Windows.Forms.TextBox
    Friend WithEvents txtDecryptedText As System.Windows.Forms.TextBox
    Friend WithEvents lblPlainText As System.Windows.Forms.Label
    Friend WithEvents lblEncryptedText As System.Windows.Forms.Label
    Friend WithEvents lblDecryptedText As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents LinkLabel2 As System.Windows.Forms.LinkLabel
    Friend WithEvents LinkLabel1 As System.Windows.Forms.LinkLabel
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
