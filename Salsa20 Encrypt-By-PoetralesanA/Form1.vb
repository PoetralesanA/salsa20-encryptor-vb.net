﻿Imports System.Text
Imports Org.BouncyCastle.Crypto
Imports Org.BouncyCastle.Crypto.Engines
Imports Org.BouncyCastle.Crypto.Parameters

Public Class Form1
    '--------------------------------------
    ' This code was created by PoetralesanA
    ' from Indonesia.
    ' YouTube channel: BrokecoderZ
    ' Developed using the 
    ' VbdotNet programming language
    '--------------------------------------

    Private Sub btnEncrypt_Click(sender As Object, e As EventArgs) Handles btnEncrypt.Click
        Try
            ' Example 128-bit key in hexadecimal format
            Dim keyHex As String = "2B7E151628AED2A6ABF7158809CF4F3C"
            Dim keyBytes As Byte() = HexToByteArray(keyHex)

            ' Example 64-bit nonce (8 bytes)
            Dim nonceBytes As Byte() = Encoding.UTF8.GetBytes("12345678")

            ' Initialize Salsa20 cipher as StreamCipher
            Dim salsa20 As New Salsa20Engine()
            Dim parameters As New ParametersWithIV(New KeyParameter(keyBytes), nonceBytes)
            Dim cipher As New BufferedStreamCipher(New Salsa20Engine())

            ' Prepare cipher for encryption with parameters
            cipher.Init(True, parameters)

            ' Example plaintext to encrypt
            Dim plaintext As String = txtPlainText.Text
            Dim plaintextBytes As Byte() = Encoding.UTF8.GetBytes(plaintext)

            ' Buffer for encrypted result
            Dim ciphertextBytes(plaintextBytes.Length - 1) As Byte

            ' Encryption process
            Dim outputLength As Integer = cipher.ProcessBytes(plaintextBytes, 0, plaintextBytes.Length, ciphertextBytes, 0)

            ' Finalize encryption process if needed
            cipher.DoFinal(ciphertextBytes, outputLength)

            ' Convert encrypted result to Base64 for display
            Dim ciphertext As String = Convert.ToBase64String(ciphertextBytes)
            MessageBox.Show("Encrypted: " & ciphertext)
            txtEncryptedText.Text = ciphertext
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message)
        End Try
    End Sub

    Private Function HexToByteArray(hex As String) As Byte()
        Dim bytes As Byte() = New Byte(hex.Length / 2 - 1) {}
        For i As Integer = 0 To hex.Length - 1 Step 2
            bytes(i / 2) = Convert.ToByte(hex.Substring(i, 2), 16)
        Next
        Return bytes
    End Function

    Private Sub btnDecrypt_Click(sender As Object, e As EventArgs) Handles btnDecrypt.Click
        Try
            ' Example 128-bit key in hexadecimal format
            Dim keyHex As String = "2B7E151628AED2A6ABF7158809CF4F3C"
            Dim keyBytes As Byte() = HexToByteArray(keyHex)

            ' Example 64-bit nonce (8 bytes)
            Dim nonceBytes As Byte() = Encoding.UTF8.GetBytes("12345678")

            ' Initialize Salsa20 cipher as StreamCipher for decryption
            Dim salsa20 As New Salsa20Engine()
            Dim parameters As New ParametersWithIV(New KeyParameter(keyBytes), nonceBytes)
            Dim cipher As New BufferedStreamCipher(New Salsa20Engine())

            ' Prepare cipher for decryption with the same parameters
            cipher.Init(False, parameters)

            ' Get encrypted text from displayed text
            Dim ciphertextBase64 As String = txtEncryptedText.Text
            Dim ciphertextBytes As Byte() = Convert.FromBase64String(ciphertextBase64)

            ' Buffer for decrypted result
            Dim decryptedBytes(ciphertextBytes.Length - 1) As Byte

            ' Decryption process
            Dim outputLength As Integer = cipher.ProcessBytes(ciphertextBytes, 0, ciphertextBytes.Length, decryptedBytes, 0)

            ' Finalize decryption process if needed
            cipher.DoFinal(decryptedBytes, outputLength)

            ' Convert decrypted result to plaintext string and display in TextBox
            Dim plaintext As String = Encoding.UTF8.GetString(decryptedBytes)
            txtDecryptedText.Text = plaintext
            MessageBox.Show("Decrypted: " & plaintext)
        Catch ex As Exception
            MessageBox.Show("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        txtEncryptedText.Clear()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        txtDecryptedText.Clear()
    End Sub

    Private Sub LinkLabel2_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        Try
            Process.Start("www.facebook.com/poetralesana")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub LinkLabel1_LinkClicked(sender As Object, e As LinkLabelLinkClickedEventArgs) Handles LinkLabel1.LinkClicked
        Try
            Process.Start("www.youtube.com/@brokecoderz")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Shown
        MsgBox("Salsa20 Encryptor" & vbNewLine & "codename : PoetralesanA", MsgBoxStyle.Information, "")
    End Sub

    Private Sub Form1_Load_1(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class
